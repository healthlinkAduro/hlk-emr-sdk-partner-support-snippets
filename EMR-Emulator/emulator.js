const fs = require('fs');
const url = require('url');
const Readable = require('stream').Readable;

function send200Response(response) {
    response.writeHead(200, {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "text/xml",
        "Transfer-Encoding": "chunked",
        "Connection": "keep-alive"
    });
}


function send500Response(response) {
    response.writeHead(500, {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "text/xml",
        "Transfer-Encoding": "chunked",
        "Connection": "keep-alive"
    });
}

function escapeRegExp(str) {
    return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

function processAttachmentRequest(request, response) {
    //get the template response
    var templateFile = fs.readFileSync('attachmentResponseTemplate.xml');
    var templateContents = templateFile.toString('utf-8');
    //tranform file to base64
    var sampleFile = fs.readFileSync('sample-attachment.rtf');
    var attachmentContent = sampleFile.toString('base64');
    //get the request body from the post request
    let body = [];
    request.on('data', (chunk) => {
        body.push(chunk);
    }).on('end', () => {
        body = Buffer.concat(body).toString();
        var formXMLFromRequest = body.substring(body.lastIndexOf("<form xmlns=\"urn:net.healthlink.genericform.model\">") + 51, body.lastIndexOf("</form>"));
        // formXMLFromRequest = formXMLFromRequest.replace('name="content"/>', 'name="content">{base64data}</content></field>');
        formXMLFromRequest = replaceAll(formXMLFromRequest,'name="content"/>', 'name="content">{base64data}</content></field>');
        // formXMLFromRequest = formXMLFromRequest.replace('{base64data}', attachmentContent);
        formXMLFromRequest = replaceAll(formXMLFromRequest,'{base64data}', attachmentContent);
        templateContents = templateContents.replace('{dynamicResponse}', formXMLFromRequest);
        const Readable = require('stream').Readable;
        const s = new Readable();
        s._read = () => {
        };
        s.push(templateContents);
        s.push(null);
        s.pipe(response);
        send200Response(response);
    });
}

require('http').createServer(function(request, response){
    var query = url.parse(request.url).pathname;
    try{
    if(request.method==='POST'){
        console.log(query);
        switch (query) {
            case '/getData':
                rs = fs.createReadStream('dataResponse.xml');
                rs.pipe(response);
                send200Response(response);
                break;
            case '/saveContainer':
                rs = fs.createReadStream('saveContainerResponse.xml');
                rs.pipe(response);
                send200Response(response);
                break;
            case '/submitForm':
                rs = fs.createReadStream('submitResponse.xml');
                rs.pipe(response);
                send200Response(response);
                break;
            case '/getAttachment':
                processAttachmentRequest(request, response);
                //
                break;
            case '/getDeliveryOptions':
                rs = fs.createReadStream('deliveryOptionsResponse.xml');
                rs.pipe(response);
                send200Response(response);
                break;
            case '/processAction':
                rs = fs.createReadStream('processActionResponse.xml');
                rs.pipe(response);
                send200Response(response);
                break;
            case '/getFormView':
                rs = fs.createReadStream('getFormViewResponse.xml');
                rs.pipe(response);
                send200Response(response);
                break;
            case '/error':
                rs = fs.createReadStream('soapException.xml');
                rs.pipe(response);
                send500Response(response);
                break;
            case '/getVersion':
                rs = fs.createReadStream('versionResponse.xml');
                rs.pipe(response);
                send200Response(response);
                break;
                default:
                response.writeHead(404);
                response.end();
        }
    }
    if(request.method==='OPTIONS'){
        response.writeHead(200,{
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "POST",
            "Access-Control-Allow-Headers": "soapaction,x-requested-with,x-prototype-version,messagetype,content-type"
        });
        response.end();
    }
    } catch (e) {
        console.log(e);
        response.end();
    }

}).listen(8080);
