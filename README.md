# hlk-emr-sdk-partner-support-snippets

##Contents

###HL7-au-samples

Sample HL7 messages.

###EMR-Emulator

Node based EMR emulator.
 
Usage:

`cd EMR-Emulator`

`node emulator.js`

**APIs**

Prepopulate Data (POST)
`http://localhost:8080/getData`


Park Form (POST)
`http://localhost:8080/saveContainer`

Submit Form (POST)
`http://localhost:8080/submitForm`

Attachment Data (POST)
`http://localhost:8080/getAttachment`

Get Version (POST)
`http://localhost:8080/getVersion`

Error Response Simulator (POST)
`http://localhost:8080/error`







